package com.martinez.headachelogger

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.martinez.headachelogger.headacheDatabase.DeleteAllRecordsASyncTask
import com.martinez.headachelogger.headacheDatabase.GetAllRecordsASyncTask


class MainActivity : AppCompatActivity() {
    private var adapter: RVAdapter? = null
    private var rv: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = "Headache Logger"

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { startLoggingActivity() }

        getRecordsAndUpdateRecycleView()


    }

    override fun onStart() {
        super.onStart()
        getRecordsAndUpdateRecycleView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        R.id.action_settings -> {
            true
        }R.id.action_deleteall->{
            DeleteAllRecordsASyncTask(this).execute()
            getRecordsAndUpdateRecycleView()
            true
        } else ->{
            super.onOptionsItemSelected(item)
        }

    }

    private fun startLoggingActivity() {
        Toast.makeText(this, "Testing method call", Toast.LENGTH_SHORT)
        val intent = Intent(this, LogHeadacheActivity::class.java)
        startActivity(intent)
    }

    private fun getRecordsAndUpdateRecycleView(){
        val task = GetAllRecordsASyncTask( this )
        rv = findViewById(R.id.headacheRV)
        val llm = LinearLayoutManager(this)
        rv!!.layoutManager = llm

        val values = task.execute().get()

        adapter = RVAdapter( values )
        rv!!.adapter = adapter
        rv!!.adapter?.notifyDataSetChanged()

        val welcomeText = findViewById<TextView>(R.id.WelcomeText)
        if(values.isNotEmpty()){
            welcomeText.visibility = INVISIBLE
        } else {
            welcomeText.visibility = VISIBLE
        }
    }
}
