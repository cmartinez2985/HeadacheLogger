package com.martinez.headachelogger

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatEditText
//import com.martinez.headachelogger.R.id.toolbar
import com.martinez.headachelogger.headacheDatabase.AddRecordASyncTask
import com.martinez.headachelogger.headacheDatabase.HeadacheData
import com.martinez.headachelogger.headacheDatabase.HeadacheDatabase
import java.text.DateFormat
import java.util.*


class LogHeadacheActivity : AppCompatActivity() {
    private var datePickerDialog: DatePickerDialog? = null
    private var whenTextInput: EditText? = null
    private var timePickerDialog: TimePickerDialog? = null
    internal var durationInput: EditText? = null
    private var month: Int = 0
    private var day: Int = 0
    private var year: Int = 0
    private var hour: Int = 0
    private var minute: Int = 0
    internal var hoursDuration: Int = 0
    internal var minutesDuration: Int = 0
    internal var pain: Int = 0

    private var eatenCB: CheckBox? = null
    private var waterCB: CheckBox? = null
    private var lightSensitiveCB: CheckBox? = null
    private var eaten: Boolean = false
    private var water: Boolean = false
    private var lightSensitive: Boolean = false
    private var notesField: String = ""

    internal var headacheDatabase: HeadacheDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.log_activity_layout)
        //val toolbar = findViewById<Toolbar>(R.id.toolbar)
        //setSupportActionBar(toolbar)
        //toolbar.title = "Log your headache"
        val fab = findViewById<FloatingActionButton>(R.id.doneFab)
        fab.setOnClickListener { addHeadacheToDB() }

        whenTextInput = findViewById(R.id.startTimeInput)
        whenTextInput!!.setOnClickListener { showDatePicker() }

        durationInput = findViewById(R.id.durationInput)
        durationInput!!.setOnClickListener { showDurationPicker() }
        //create Date Picker
        datePickerDialog = DatePickerDialog(this)
        datePickerDialog!!.setOnDateSetListener { _ /*View is never used?*/, _year, _month, _dayOfMonth ->
            month = _month
            year = _year
            day = _dayOfMonth
            showTimePicker()
        }

        val timeSetListener = TimePickerDialog.OnTimeSetListener { _ /*View is never used?*/, hourOfDay, _minute ->
            hour = hourOfDay
            minute = _minute
            val df = DateFormat.getDateTimeInstance()
            val cal = Calendar.getInstance().clone() as Calendar
            cal.set(year, month, day, hour, minute)
            val dateText = df.format(cal.timeInMillis)
            whenTextInput!!.setText(dateText)
        }
        val cal = Calendar.getInstance()
        timePickerDialog = TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false)
        setSeekBarListener()
        setCheckBoxListeners()
        Thread {
            headacheDatabase = HeadacheDatabase.getInstance(this)
        }.start()
    }

    private fun setCheckBoxListeners() {
        eatenCB = findViewById(R.id.eatenCB)
        waterCB = findViewById(R.id.waterCB)
        lightSensitiveCB = findViewById(R.id.lightSensitiveCB)

        eatenCB!!.setOnCheckedChangeListener { compoundButton, b ->
            eaten = b
        }

        waterCB!!.setOnCheckedChangeListener { compoundButton, b ->
            water = b
        }

        lightSensitiveCB!!.setOnCheckedChangeListener { compoundButton, b ->
            lightSensitive = b
        }
    }
    private fun setSeekBarListener(){
        val painSeekBar = findViewById<SeekBar>(R.id.painSeekBar)
        painSeekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                pain = p1
            }

            override fun onStartTrackingTouch(p0: SeekBar?) { /*ignore*/ }

            override fun onStopTrackingTouch(p0: SeekBar?) { /*ignore*/ }
        })
    }

    private fun showDatePicker() {
        datePickerDialog!!.show()
    }

    private fun showTimePicker() {
        timePickerDialog!!.show()
    }

    private fun showDurationPicker() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("How long did the headache persist?")
                .setTitle("Duration of headache")
                .setPositiveButton(R.string.ok) { dialog, id ->
                    // User clicked OK button
                    durationInput!!.setText( StringBuilder().append(hoursDuration).append(" Hours ").append(minutesDuration).append(" Minutes" ).toString())
                    dialog.dismiss()
                }
        builder.setNegativeButton(R.string.cancel) { dialog, id ->
            // User cancelled the dialog
            dialog.dismiss()
        }

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.duration_layout, null)
        builder.setView(dialogView)

        val durationDialog = builder.create()
        val hourPicker = dialogView.findViewById<NumberPicker>( R.id.Hours )
        var hourValues = arrayOfNulls<String>(25)
        for(i in hourValues.indices ){
            hourValues[i] = i.toString()
        }

        val minuteValues = arrayOfNulls<String>(60)
        for(i in minuteValues.indices ){
            minuteValues[i] = i.toString()
        }
        hourPicker.displayedValues = (hourValues)
        hourPicker.minValue = (0)
        hourPicker.maxValue = (24)
        hoursDuration = 0
        hourPicker.setOnValueChangedListener { picker, oldVal, newVal -> hoursDuration = newVal }
        val minutesPicker = dialogView.findViewById<NumberPicker>( R.id.minutes )
        minutesPicker.displayedValues = (minuteValues)
        minutesPicker.minValue = (0)
        minutesPicker.maxValue = (59)
        minutesDuration = 0
        minutesPicker.setOnValueChangedListener { picker, oldVal, newVal -> minutesDuration = newVal }
        durationDialog.show()

    }

    private fun addHeadacheToDB(){
        var otherNotesTextArea: AppCompatEditText = findViewById(R.id.otherNotes)
        notesField = otherNotesTextArea.text.toString()

        val cal = Calendar.getInstance().clone() as Calendar
        cal.set(year, month, day, hour, minute)
        val headacheData = HeadacheData( null, cal.timeInMillis, hoursDuration, minutesDuration, pain, water, eaten, lightSensitive, notesField)
        val addRecord = AddRecordASyncTask(this )
        if (addRecord.execute( headacheData ).get() ){
            toast("Record added!")
        } else {
            toast("Record not added!")
        }
        finish()
    }

    private fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
