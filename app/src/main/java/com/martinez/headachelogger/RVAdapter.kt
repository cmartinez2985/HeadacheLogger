package com.martinez.headachelogger

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.martinez.headachelogger.RVAdapter.HeadacheViewHolder

import com.martinez.headachelogger.headacheDatabase.HeadacheData
import java.text.DateFormat
import java.util.*

/**
 * Created by chris on 3/11/18.
 */

class RVAdapter constructor(headaches: List<HeadacheData>) : RecyclerView.Adapter<RVAdapter.HeadacheViewHolder>() {
    private var headaches: List<HeadacheData>

    class HeadacheViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        internal var cv: CardView
        internal var headacheTime: TextView
        internal var headacheDurationHours: TextView
        internal var headacheDurationMinutes: TextView
        internal var headachePainIntensity: TextView
        internal var headacheOtherNotes: TextView
        internal var waterText: TextView
        internal var headacheId: Long? = 0
        init {
            cv = itemView.findViewById<View>(R.id.cv) as CardView
            headacheTime = itemView.findViewById<View>(R.id.headacheTime) as TextView
            headacheDurationHours = itemView.findViewById<View>(R.id.headacheDurationHours) as TextView
            headacheDurationMinutes = itemView.findViewById<View>(R.id.headacheDurationMinutes) as TextView
            headachePainIntensity = itemView.findViewById<View>(R.id.headachePainIntensity) as TextView
            headacheOtherNotes = itemView.findViewById<View>(R.id.headacheOtherNotes) as TextView
            waterText = itemView.findViewById<View>(R.id.waterText) as TextView
        }

    }

    init {
        this.headaches = headaches
    }

    override fun getItemCount(): Int {
        return headaches.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): HeadacheViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.content_main, viewGroup, false)
        return HeadacheViewHolder(v)
    }

    override fun onBindViewHolder(headacheViewHolder: HeadacheViewHolder, i: Int) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = headaches[i].timeInMillis
        val df = DateFormat.getDateTimeInstance()
        val dateText = df.format(calendar.timeInMillis)

        headacheViewHolder.headacheTime.text = dateText
        headacheViewHolder.headacheDurationHours.text = Integer.toString(headaches[i].hoursDuration)
        headacheViewHolder.headacheDurationMinutes.text = Integer.toString(headaches[i].minutesDuration)
        headacheViewHolder.headachePainIntensity.text = Integer.toString(headaches[i].painIntensity)
        headacheViewHolder.waterText.text = headaches[i].water.toString()
        headacheViewHolder.headacheOtherNotes.text = headaches[i].otherNotes
        headacheViewHolder.headacheId = headaches[i].id
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }
}