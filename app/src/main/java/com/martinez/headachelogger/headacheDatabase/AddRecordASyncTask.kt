package com.martinez.headachelogger.headacheDatabase

import android.content.Context
import android.os.AsyncTask
import android.os.Looper
import android.widget.Toast

/**
 * Created by chris on 3/11/18.
 */
class AddRecordASyncTask( _context: Context ): AsyncTask<HeadacheData, Void, Boolean>(){
    private var context: Context = _context
    override fun doInBackground(vararg _headacheData: HeadacheData?): Boolean {
        val headacheDB = HeadacheDatabase.getInstance( context )
        for(item in _headacheData) {
            if (item != null) {
                headacheDB?.headacheDataDao()?.insert(item)
            } else {
                //Toast.makeText(context, "Some kind of null data was being added to the DB!", Toast.LENGTH_SHORT)
                return false
            }
            //Toast.makeText(context, StringBuilder().append("Headache data: ").append(item).append(" was added to the DB!"), Toast.LENGTH_SHORT)
        }
        return true
    }

}