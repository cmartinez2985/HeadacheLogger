package com.martinez.headachelogger.headacheDatabase

import android.content.Context
import android.os.AsyncTask
import android.os.Looper

/**
 * Created by chris on 3/11/18.
 */
class GetAllRecordsASyncTask( _context: Context): AsyncTask<Void, Void, List<HeadacheData>>(){
    internal val context: Context = _context
    override fun doInBackground(vararg params: Void?): List<HeadacheData>{
        val headacheDB = HeadacheDatabase.getInstance( context )
        val headacheRows = headacheDB?.headacheDataDao()?.getAllAscending()

        if( headacheRows != null ) {
            return headacheRows
        }

        System.out.println("Database is empty or something bad happened")
        return emptyList()
    }
}