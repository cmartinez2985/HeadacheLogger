package com.martinez.headachelogger.headacheDatabase

import android.content.Context
import android.os.AsyncTask
import android.os.Looper

/**
 * Created by chris on 3/11/18.
 */
class DeleteAllRecordsASyncTask( _context: Context): AsyncTask<Void, Void, Boolean>(){
    internal val context: Context = _context
    override fun doInBackground(vararg params: Void?) : Boolean{
        val headacheDB = HeadacheDatabase.getInstance( context )
        headacheDB?.headacheDataDao()?.deleteAll()
        return true
    }
}