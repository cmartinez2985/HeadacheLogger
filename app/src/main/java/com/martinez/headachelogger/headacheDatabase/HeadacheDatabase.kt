package com.martinez.headachelogger.headacheDatabase

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context

/**
 * Created by chris on 3/11/18.
 */
@Database(entities = arrayOf(HeadacheData::class), version = 1)

abstract class HeadacheDatabase : RoomDatabase() {
    abstract fun headacheDataDao(): HeadacheDataDao

    companion object {
        private var INSTANCE: HeadacheDatabase? = null

        fun getInstance(context: Context): HeadacheDatabase? {
            if (INSTANCE == null) {
                synchronized(HeadacheDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            HeadacheDatabase::class.java, "headacheData.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
