package com.martinez.headachelogger.headacheDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by chris on 3/11/18.
 */

@Entity(tableName="headacheData")
data class HeadacheData(@PrimaryKey(autoGenerate=true) var id: Long?,
                        @ColumnInfo(name = "timestamp") var timeInMillis: Long,
                        @ColumnInfo(name = "hoursDuration") var hoursDuration: Int,
                        @ColumnInfo(name = "minutesDuration") var minutesDuration: Int,
                        @ColumnInfo(name = "painIntensity") var painIntensity: Int,
                        @ColumnInfo(name = "water") var water: Boolean,
                        @ColumnInfo(name = "eaten") var eaten: Boolean,
                        @ColumnInfo(name = "lightSensitive") var lightSensitive: Boolean,
                        @ColumnInfo(name = "Other Notes") var otherNotes: String ){
    constructor():this(null,0,0,0,0, false, false, false, "")

    override fun toString(): String {
        return "HeadacheData(id=$id, timeInMillis=$timeInMillis, hoursDuration=$hoursDuration, minutesDuration=$minutesDuration, painIntensity=$painIntensity, otherNotes='$otherNotes')"
    }

}
