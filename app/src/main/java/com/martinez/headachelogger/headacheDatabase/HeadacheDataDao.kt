package com.martinez.headachelogger.headacheDatabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

/**
 * Created by chris on 3/11/18.
 */
@Dao
interface HeadacheDataDao{
    @Query("SELECT * from headacheData")
    fun getAll(): List<HeadacheData>

    @Query("SELECT * from headacheData ORDER BY id DESC")
    fun getAllAscending(): List<HeadacheData>

    @Insert(onConflict = REPLACE)
    fun insert(headacheData: HeadacheData)

    @Query("DELETE from headacheData")
    fun deleteAll()
}